import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.*;

public class SixthProgram2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s;
        while (true) {
            System.out.println("Enter String:");
            s = in.nextLine();
            Pattern pattern = Pattern.compile("([0-9])+");
            Matcher matcher = pattern.matcher(s);

            while (matcher.find()) {
                System.out.println(matcher.group());
            }
        }
    }

}