package com.example.demo;
import java.lang.*;
import java.util.*;

//import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang3.RandomStringUtils;


@RestController
public class Controller {

    @Value("${length}")
    private int length;

//    public String GenerateString(int n) {
//        Random rand = new Random();
//        int upperlimit = 25;
//        int i,k;
//        StringBuffer r = new StringBuffer(n);
//        String ans ="";
//        for(i=0;i<n;i++) {
//             k = rand.nextInt(upperlimit);
//             int  c = (int)'a'+k;
//             r.append((char)c);
//        }
//        return r.toString();
//    }

    @RequestMapping(method = RequestMethod.POST,value = "/")
    public ResponseEntity<String[]> get(@RequestBody Count obj){

        String[] arr = new String[obj.getCount()];
        for(int i=0;i<obj.getCount();i++) {
            arr[i]=  RandomStringUtils.random(length,true,false);
        }
        for(int i=0;i<obj.getCount();i++){
            System.out.println(arr[i]);
        }
        return new ResponseEntity<>(arr,HttpStatus.OK);
    }
}
