import java.lang.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

class FifthProgram {
    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        DateFormat dateFormat = new SimpleDateFormat("dd");
        DateFormat monthFormat = new SimpleDateFormat("MMM");
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        System.out.println(yearFormat.format(cal.getTime()) + "-" + monthFormat.format(cal.getTime()) + "-"
                + dateFormat.format(cal.getTime()));

    }
}