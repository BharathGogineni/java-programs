import java.util.*;
import java.lang.*;

class Person {
    String lastName;
    String firstName;
    int age;

    Person(String personLastName, String personFirstName, int personAge) {
        this.lastName = personLastName;
        this.firstName = personFirstName;
        this.age = personAge;
    }
}

class PersonComparator implements Comparator<Person> {

    public int compare(Person person1, Person person2) {

        int firstNameCompare = person1.firstName.compareTo(person2.firstName);
        int lastNameCompare = person1.lastName.compareTo(person2.lastName);
        if (firstNameCompare == 0) {
            if (lastNameCompare == 0) {
                return firstNameCompare;
            } else {
                return lastNameCompare;
            }
        } else {
            return firstNameCompare;
        }
    }

}

class FirstProgram {
    public static void main(String[] args) {
        String c = "a";
        ArrayList<Person> arr = new ArrayList<>();
        System.out.println("Select an operation");
        System.out.println("1) press \"a\" to add one more customer");
        System.out.println("2) Press \"b\" to show the persons");
        System.out.println("3) press \"c\" to exit");
        Scanner in = new Scanner(System.in);
        c = in.nextLine();
        System.out.println("the string entered is " + c);
        while (c != "c") {
            if (c.equals("a")) {

                String lastName, firstName;
                int age;
                System.out.println("Enter firstname");
                firstName = in.nextLine();
                System.out.println("Enter lastname");
                lastName = in.nextLine();
                System.out.println("Enter age");
                age = in.nextInt();
                Person persontobeadded = new Person(firstName, lastName, age);
                arr.add(persontobeadded);
            } else if (c.equals(("b"))) {
                Collections.sort(arr, new PersonComparator());
                for (int i = 0; i < arr.size(); i++) {
                    System.out.println(
                            arr.get(i).firstName + " " + arr.get(i).lastName + " " + Integer.toString(arr.get(i).age));
                }
            } else if (c.equals("c")) {
                break;
            } else {
                System.out.println("Select an operation");
                System.out.println("1) press \"a\" to add one more customer");
                System.out.println("2) Press \"b\" to show the persons");
                System.out.println("3) press \"c\" to exit");
            }
            c = in.nextLine();
        }

    }
}
