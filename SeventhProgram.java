import java.util.*;
import java.lang.*;
import java.net.URL;
import java.io.IOException;
import java.io.FileWriter;

class SeventhProgram {
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://example1.in");
        try {
            Scanner sc = new Scanner(url.openStream());
            String s = "";
            while (sc.hasNext()) {
                s = s + sc.next();
            }

            try (FileWriter fw = new FileWriter("./output")) {
                fw.append(s);

            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}