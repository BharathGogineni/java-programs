//In the person class, add Aadhar no. Two persons are same if they have same Aadhar no. Put them in a set such that
// duplicates are handled automatically. In addition, they should be sorted by Aadhar and Name

import java.util.*;

import java.lang.*;

class Person1 {
    int aadharNo;
    String name;

    Person1(int personAadhar, String personName) {
        this.aadharNo = personAadhar;
        this.name = personName;
    }
}

class PersonComparator1 implements Comparator<Person1> {

    public int compare(Person1 p1, Person1 p2) {
        if (p1.aadharNo < p2.aadharNo)
            return -1;
        else if (p2.aadharNo < p1.aadharNo)
            return 1;
        else
            return 0;
    }
}

class FourthProgram {
    public static void main(String[] args) {
        System.out.println("Select an operation");
        System.out.println("1.)Enter \"a\"  to add person");
        System.out.println("2.)Enter \"b\" to show the set");
        System.out.println("3.)Enter \"c\" to exit");
        Scanner in = new Scanner(System.in);
        String s;
        s = in.nextLine();
        SortedSet<Person1> set = new TreeSet<Person1>(new PersonComparator1());

        while (!s.equals("c")) {
            if (s.equals("a")) {
                System.out.println("Enter persons Aadhar no");
                int Aadharno = in.nextInt();
                String enter = in.nextLine();
                System.out.println("Enter persons Name");
                String name = in.nextLine();
                Person1 newperson = new Person1(Aadharno, name);
                set.add(newperson);

            } else if (s.equals("b")) {
                for (Person1 element : set) {

                    System.out.println(Integer.toString(element.aadharNo) + " " + element.name);
                }
            } else if (s.equals("c")) {
                break;
            }
            System.out.println("Select an operation");
            System.out.println("1.)Enter \"a\"  to add person");
            System.out.println("2.)Enter \"b\" to show the set");
            System.out.println("3.)Enter \"c\" to exit");

            s = in.nextLine();
        }
    }
}