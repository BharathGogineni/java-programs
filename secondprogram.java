import java.util.*;

import javax.sound.sampled.SourceDataLine;

import java.lang.*;
import java.lang.reflect.Array;

class Person {
    String firstName;
    String lastName;
    int age;
    ArrayList<Person> arr;

    Person(String personFirstName, String personLastName, int personAge) {
        this.firstName = personFirstName;
        this.lastName = personLastName;
        this.age = personAge;
        this.arr = new ArrayList<>();
    }

}

class SecondProgram {
    ArrayList<String> firstnamearray = new ArrayList<>();
    ArrayList<String> secondnamearray = new ArrayList<>();
    int SIZE = 14;

    void initialise() {
        int i;
        for (i = 0; i < SIZE; i++) {
            int j = i + 1;
            String s = "firstname" + Integer.toString(j);
            firstnamearray.add(s);
            s = "lastname" + Integer.toString(j);
            secondnamearray.add(s);
        }
    }

    void createTree(Person p, int i) {
        if ((2 * i + 1) >= SIZE)
            return;
        Person p1 = new Person(firstnamearray.get(2 * i + 1), secondnamearray.get(2 * i + 1), 2 * i + 1);
        createTree(p1, 2 * i + 1);
        p.arr.add(p1);
        if ((2 * i + 2) >= SIZE)
            return;
        Person p2 = new Person(firstnamearray.get(2 * i + 2), secondnamearray.get(2 * i + 2), 2 * i + 2);
        createTree(p2, 2 * i + 2);
        p.arr.add(p2);
    }

    int count = 0;

    void dfs(Person p) {
        count++;
        for (int i = 0; i < count; i++)
            System.out.print("->");
        System.out.println(p.firstName + " " + p.lastName);
        for (int i = 0; i < p.arr.size(); i++) {
            dfs(p.arr.get(i));
        }
        count--;
    }

    public static void main(String[] args) {

        SecondProgram ans = new SecondProgram();
        ans.initialise();
        Person p = new Person(ans.firstnamearray.get(0), ans.secondnamearray.get(0), 0);
        ans.createTree(p, 0);
        ans.dfs(p);
    }
}